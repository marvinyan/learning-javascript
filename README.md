Resources
---
### Core
- Guide: [How to Learn JavaScript Properly](http://javascriptissexy.com/how-to-learn-javascript-properly/)
- Study Groups
	- [Reddit (Oct - Dec 2013)](http://www.reddit.com/r/learnjavascript/comments/1oq8ns/learn_javascript_properly_omnibus_post_for_the/)
	- [Reddit Seattle](http://www.reddit.com/r/learn_js_in_seattle/)
	- [Reddit (March - May 2014)](http://www.reddit.com/r/learnjavascript/comments/2179b5/learn_javascript_properly_omnibus_post_for_the/)
- Books:
	- [Professional JavaScript for Web Developers](http://www.amazon.com/gp/product/1118026691)
	- [JavaScript: The Definitive Guide](http://www.amazon.com/gp/product/0596805527)
		- [Patrick Gillespie's notes for the rhino book](http://readingtherhinojsbook.wordpress.com/category/chapters/)

### Learn
- [Codecademy](http://codecademy.com/)
- [Try jQuery](http://try.jquery.com/)
- [Using WebStorm Effectively](http://2oahu.com/posts/2013/webstorm-javascript/)
- [10 Things I Learned from the jQuery source](http://www.paulirish.com/2010/10-things-i-learned-from-the-jquery-source/) by Paul Irish

### Practice
- [Codewars](http://www.codewars.com/)
- [Project Euler](https://projecteuler.net/problems)