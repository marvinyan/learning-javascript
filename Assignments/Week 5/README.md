Week 5 Assignment
---
The goal of this assignment was to improve the quiz made during week 4.

- Client-side data validation ensures that the user answers each question before proceeding.
- A back button has been implemented to allow the user to change previous answers.
- Additional jQuery animations were added. 

I've also improved the UI from week 4. [Lato](http://typecast.com/preview/google/Lato) is such a fine font ...

##Links
- <a href='http://jsfiddle.net/marvinyan/tjXL8/show/'>Page preview</a>
- <a href='http://jsfiddle.net/marvinyan/tjXL8' target='_blank'>JSFiddle with code</a>
- Paul Irish's [10 Things I Learned from the jQuery source](http://www.paulirish.com/2010/10-things-i-learned-from-the-jquery-source/)