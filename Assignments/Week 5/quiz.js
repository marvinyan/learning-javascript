// jsHint stuff
/*global $:false*/
$(document).ready(function () {
  "use strict";

  // Center contents on resize
  // http://stackoverflow.com/a/13903744/3505851
  function adjustLayout() {
    var ele = $("body");
    $(ele).css({
      position: "absolute",
      left: ($(window).width() - $(ele).outerWidth()) / 2,
      top: ($(window).height() - $(ele).outerHeight()) / 3
    });
  }

  $(window).resize(adjustLayout).resize();

  var score, current, allQuestions, answered;
  score = current = 0;
  allQuestions = [
    {question: "Triton is a moon of which planet?",
      choices: ["Uranus", "Neptune", "Saturn", "Jupiter"],
      correctAnswer: "1"},
    {question: "Which planet has the largest volcano in the solar system?",
      choices: ["Saturn", "Venus", "Jupiter", "Mars"],
      correctAnswer: "3"},
    {question: "What substance produced from the plant is used to create its " +
        "cell walls?",
      choices: ["Lignin", "Ligature", "Cellulose", "Chlorophyll"],
      correctAnswer: "2"},
    {question: "Which of these constellations represents a goat?",
      choices: ["Cepheus", "Capricornus", "Taurus", "Cancer"],
      correctAnswer: "1"},
    {question: "What is the scientific name for red blood cells?",
      choices: ["Erythrocytes", "Leukocytes", "Thrombocytes", "Megakaryocyte"],
      correctAnswer: "0"}
  ];
  answered = [];

  function prevButtonCheck() {
    // Enable/disable previous button if current index is 0 (false)
    if (!current) {
      $("#prev").addClass("disabled");
    } else {
      $("#prev").removeClass("disabled");
    }
  }

  // Handles prev and next logic
  // finish() ends any currently running animation (prevent "jank" when mashing
  // previous and next)
  function nextQuestion() {
    // hide current question, replace text, then show it
    $("#questionNum").finish().hide().text("Question " +
        (current + 1)).fadeIn();
    $("#question").finish().hide().text(allQuestions[current].question)
        .fadeIn();

    // Iterate through current question's choices.
    // Set first choice's text on webpage to choices[0], etc
    // jQuery.each() used, not .each()
    $.each(allQuestions[current].choices, function (index, value) {
      $("label").eq(index).finish().hide().text(value).fadeIn();
    });

    // Clear previous checked radio, replace with saved position
    $("input:checked").prop("checked", false);
    $("input").eq(answered[current]).prop("checked", true);

    prevButtonCheck();
  }

  function endQuiz() {
    for (var i = 0; i < allQuestions.length; i++) {
      if (answered[i] === allQuestions[i].correctAnswer) {
        score++;
      }
    }

    $("#questionNum").hide().text("Quiz completed!").fadeIn();
    $("#question").hide().text("You scored " + score + "/" +
        allQuestions.length).fadeIn();
    $("#nav").slideUp("slow");
  }

  $("#submit").on("click", function () {
    var userAnswer = $("input:checked").val();

    if (userAnswer !== undefined) {
      answered[current] = userAnswer;

      current++;
      if (current !== allQuestions.length) {
        nextQuestion();
      } else {
        endQuiz();
      }
    } else {
      $("#error").finish().fadeIn("slow").delay("1200").fadeOut();
    }
  });

  $("#prev").on("click", function () {
    if (current) {
      current--;
      nextQuestion();
    }
  });

  nextQuestion();
  adjustLayout();
});