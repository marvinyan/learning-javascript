Week 3 Assignment
---
The goal of this assignment was to create an HTML form with an input box that would display any submitted text on the webpage.

Tinkering with the CSS and implementing the auto scrolling script was quite refreshing after having completed the base JavaScript/jQuery code.

##Links
- <a href='http://jsfiddle.net/marvinyan/uYYqP/show' target='_blank'>Page preview</a>
- <a href='http://jsfiddle.net/marvinyan/uYYqP/' target='_blank'>JSFiddle with code</a>