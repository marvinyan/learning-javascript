$(document).ready(function () {
    $('#inputBox').keypress(function (e) {
        if (e.which == 13) { showMessage(); }
    });
    $('#submitButton').on('click', function() {
        showMessage();
    });
});

function showMessage() {
    var message = $('#inputBox').val();
    if (message !== "") {
        $('#displayArea ul').append('<li>' + '<div class="timestamp">' + getTimestamp() + '</div>' + message + '</li></br>');
        $('#inputBox').val('');
    }
    var display = document.getElementById('displayArea');
    display.scrollTop = display.scrollHeight;
}
    
function getTimestamp() {
    var today = new Date();
    var hours = today.getHours();
    var minutes = today.getMinutes();
    var seconds = today.getSeconds();
    minutes = fixFormat(minutes);
    seconds = fixFormat(seconds);
    var period = "am";
    if (hours >= 12) { period = "pm"; }
    /* http://stackoverflow.com/questions/14415618/replace-military-time-to-normal-time-with-javascript */
    hours = ((hours + 11) % 12) + 1; 
    return hours + ":" + minutes + ":" + seconds + " " + period;
}

function fixFormat(num) {
    if (num < 10) {
        num = "0" + num;
    }
    return num;
}