// jsHint stuff
/*global $:false*/
"use strict";
$(document).ready(function() {
  var allQuestions, counter, score;

  counter = score = 0;
  allQuestions = [
    {question: "Triton is a moon of which planet?",
      choices: ["Uranus", "Neptune", "Saturn", "Jupiter"],
      correctAnswer: "1"},
    {question: "Which planet has the largest volcano in the solar system?",
      choices: ["Saturn", "Venus", "Jupiter", "Mars"],
      correctAnswer: "3"},
    {question: "What substance produced from the plant is used to create its cell walls?",
      choices: ["Lignin", "Ligature", "Cellulose", "Chlorophyll"],
      correctAnswer: "2"},
    {question: "Which of these constellations represents a goat?",
      choices: ["Cepheus", "Capricornus", "Taurus", "Cancer"],
      correctAnswer: "1"},
    {question: "What is the scientific name for red blood cells?",
      choices: ["Erythrocytes", "Leukocytes", "Thrombocytes", "Megakaryocyte"],
      correctAnswer: "0"}];

  function nextQuestion() {
    $("#questionNum").text("Question " + (counter + 1));
    $("#question").text(allQuestions[counter].question);

    // Iterate through current question's choices.
    // Set first choice's text on webpage to choices[0], etc
    // jQuery.each() used, not .each()
    $.each(allQuestions[counter].choices, function(index, value) {
      $("label").eq(index).text(value);
    });
  }

  function getRadioValue() {
    return $("input:checked").val();
  }

  function endQuiz() {
    $("#questionNum").text("Quiz completed!");
    $("#question").text("You scored " + score + " / " + allQuestions.length);
    $(".submit").slideUp("slow");
  }

  $(".submit").on("click", function() {
    if (getRadioValue() !== undefined) {
      if (getRadioValue() === allQuestions[counter].correctAnswer) {
        score++;
      }
      counter++;
      if (counter !== 5) {
        nextQuestion();
        $("input:checked").prop("checked", false);
      } else {
        endQuiz();
      }
    } else {
      $("#error").slideDown("slow").delay("1200").fadeOut();
    }
  });

  nextQuestion();
});