Week 2 Assignment
---
Supplementing the basic JavaScript learned from [Codecademy](http://www.codecademy.com/) with problems [1](https://projecteuler.net/problem=1) and [2](https://projecteuler.net/problem=2) of Project Euler.
