// jsHint stuff
/*global $:false*/
"use strict";

var score = 0,
    current = 0,
    allQuestions = [],
    answered = [];

// Center contents on resize
// http://stackoverflow.com/a/13903744/3505851
function adjustLayout() {
  var $authContainer = $("#auth-container"),
      $signupContainer = $("#signup-container"),
      $quizContainer = $("#quiz-container"),
      $welcomeContainer = $("#welcome-container");

  $authContainer.css({
    position: "absolute",
    "min-width": "25%",
    width: "350px",
    left: ($(window).width() - $authContainer.outerWidth()) / 2,
    top: ($(window).height() - $authContainer.outerHeight()) / 2
  });

  $signupContainer.css({
    position: "absolute",
    "min-width": "50%",
    left: ($(window).width() - $signupContainer.outerWidth()) / 2,
    top: ($(window).height() - $signupContainer.outerHeight()) / 2
  });

  $quizContainer.css({
    position: "absolute",
    width: "360px",
    "min-width": "25%",
    left: ($(window).width() - $quizContainer.outerWidth()) / 2,
    top: ($(window).height() - $quizContainer.outerHeight()) / 2
  });

  $welcomeContainer.css({
    position: "absolute",
    "min-width": "90%",
    left: ($(window).width() - $welcomeContainer.outerWidth()) / 2,
    top: ($(window).height() - $welcomeContainer.outerHeight()) / 2
  });
}
$(window).resize(adjustLayout).resize();

// Natively get and parse JSON
(function() {
  var xhr = new XMLHttpRequest();

  // Calls on allQuestions[] occur before data is retrieved
  // Setting async to false will block progress until file is
  // retrieved and parsed
  xhr.open("GET", "questions.json", false);
  xhr.onreadystatechange = function() {
    if (this.readyState === 4 && this.status === 200) {
      allQuestions = JSON.parse(this.responseText);
    }
  };
  xhr.send();
})();

// Alternative way to get JSON
// jQuery's $.getJSON is also async by default.
/*
 $.ajax({
 url: "questions.json",
 type: "GET",
 dataType: "json",
 async: false,
 success: function(data) {
 allQuestions = data;
 }
 });
 */

function prevButtonCheck() {
  // Disable previous button if current index is 0 (false)
  if (!current) {
    $("#prev").addClass("disabled");
  } else {
    $("#prev").removeClass("disabled");
  }
}

// Handles prev and next logic
// finish() ends any currently running animation (prevent "jank" when mashing
// previous and next)
function nextQuestion() {
  // hide current question, replace text, then show it
  $("#questionNum").finish().hide().text("Question " +
      (current + 1)).fadeIn();
  $("#question").finish().hide().text(allQuestions[current].question)
      .fadeIn();

  // Iterate through current question's choices.
  // Set first choice's text on webpage to choices[0], etc
  // jQuery.each() used, not .each()
  $.each(allQuestions[current].choices, function(index, value) {
    $(".radio > label").eq(index).finish().hide().text(value).fadeIn();
  });

  // Clear previous checked radio, replace with saved position
  $(".radio > input:checked").prop("checked", false);
  $(".radio > input").eq(answered[current]).prop("checked", true);

  prevButtonCheck();
}

function endQuiz() {
  for (var i = 0; i < allQuestions.length; i++) {
    if (answered[i] === allQuestions[i].correctAnswer) {
      score++;
    }
  }

  $("#questionNum").hide().text("Quiz completed!").fadeIn();
  $("#question").hide().text("You scored " + score + "/" +
      allQuestions.length).fadeIn();

  // Handle button mashing before fully hidden
  $("#prev").unbind("click");
  $("#submit-quiz").unbind("click");
  $("#nav").slideUp("slow");
}

function bindSubmit() {
  $("#submit-quiz").on("click", function() {
    var userAnswer = $(".radio > input:checked").val();
    if (userAnswer !== undefined) {
      answered[current] = userAnswer;
      current++;
      if (current !== allQuestions.length) {
        nextQuestion();
      } else {
        endQuiz();
      }
    } else {
      $("#quiz-error").finish().fadeIn("slow").delay(1000).fadeOut();
    }
  });
}

// Do nothing if 'previous' button is clicked on first question
function bindPrev() {
  $("#prev").on("click", function() {
    if (current) {
      current--;
      nextQuestion();
    }
  });
}

function resetQuiz() {
  endQuiz();
  answered = [];
  current = 0;
  score = 0;
  bindSubmit();
  bindPrev();
  $("#nav").slideDown(function() {
    adjustLayout(); // Quiz UI always gets pushed around
  });
  nextQuestion();
}

function setCookie(user) {
  document.cookie = "username=" + user;
}

function unsetCookie() {
  document.cookie += "; expires=Thu, 1 Jan 1970 00:00:00 GMT";
}

function getCookie(cname) {
  var name = cname + "=";
  var cookieArray = document.cookie.split(";");
  for (var i = 0; i < cookieArray.length; i++) {
    var cookie = cookieArray[i].trim();
    if (!cookie.indexOf(name)) {
      return cookie.substring(name.length, cookie.length);
    }
  }
  return "";
}

function isLoggedIn() {
  return (getCookie("username") !== "");
}

function showLoggedInButtons() {
  $(".pre-login").hide();
  $("#logout").show();
  var localUser = JSON.parse(localStorage.getItem(getCookie("username")));
  $("#profile").text(localUser.firstName + " " +
      localUser.lastName).show();
}

function hideLoggedInButtons() {
  $(".pre-login").show();
  $("#logout").hide();
  $("#profile").hide();
}

function greet() {
  if (isLoggedIn()) {
    $("#greeting").text("Welcome back, " +
        JSON.parse(localStorage.getItem(getCookie("username"))).firstName +
        ".");
    $("#greeting-box").fadeIn("slow").delay(1200).fadeOut();
  }
}

// Handle hiding and showing pages
function showPage(page) {
  // Caching jQuery selectors
  var $authContainer = $("#auth-container");
  var $signupContainer = $("#signup-container");
  var $quizContainer = $("#quiz-container");
  var $welcomeContainer = $("#welcome-container");

  $authContainer.hide();
  $signupContainer.hide();
  $quizContainer.hide();
  $welcomeContainer.hide();

  switch (page)
  {
    case "auth":
      $authContainer.fadeIn();
      break;
    case "signup":
      $signupContainer.fadeIn();
      break;
    case "quiz":
      resetQuiz();
      $quizContainer.fadeIn();
      break;
    case "welcome":
      $welcomeContainer.fadeIn();
      break;
    case "home":
      if (isLoggedIn()) {
        showLoggedInButtons();
        showPage("quiz");
      } else {
        showPage("welcome");
        hideLoggedInButtons();
      }
      break;
  }
}

function auth(user, pass) {
  var localUser = JSON.parse(localStorage.getItem(user));
  if (localUser !== null && pass === localUser.pass) {
    setCookie(user);
    greet();
    showLoggedInButtons();
    return true;
  }
  return false;
}

// Store user info as JSON string
function register(user, pass, firstName, lastName) {
  var userInfo = {
    "pass": pass,
    "firstName": firstName,
    "lastName": lastName
  };

  localStorage.setItem(user, JSON.stringify(userInfo));
  setCookie(user);
  showPage("quiz");
  showLoggedInButtons();
}

// Press enter to submit forms
$(".enter-field").keypress(function (e) {
  if (e.which === 13) {
    if ($("#signup-container").is(":visible")) {
      $("#submit-signup").click();
    } else {
      $("#submit-login").click();
    }
  }
});

// Home pressed
$("#home").on("click", function() {
  showPage("home");
});

// Sign Up pressed
$("#signup").on("click", function() {
  showPage("signup");
});

// Login pressed
$("#login").on("click", function() {
  showPage("auth");
});

// Login button handler
$("#submit-login").on("click", function() {
  var $user = $("#user");
  var $pass = $("#pass");

  if (!auth($user.val(), $pass.val())) {
    $("#login-error").finish().slideDown().delay(1200).slideUp();
  } else {
    showPage("quiz");
  }

  // Clear login form
  $user.val("");
  $pass.val("");
});

// Logout pressed
$("#logout").on("click", function() {
  unsetCookie();
  showPage("home");
});

// Submit sign up form
$("#submit-signup").on("click", function() {
  // No validation ... yet
  var $user = $("#user-su");
  var $pass = $("#pass-su");
  var $fn = $("#first-name");
  var $ln = $("#last-name");

  register(
      $user.val(),
      $pass.val(),
      $fn.val(),
      $ln.val()
  );

  // Clear sign up form
  $user.val("");
  $pass.val("");
  $fn.val("");
  $ln.val("");
});

$(document).ready(function() {
  // If cookie name matches storage name, then show greeting toast and quiz.
  // Hide Login/Sign and show Logout.
  // If cookie is not present, then show welcome screen and Login/Sign up.
  // If auth() is true, then setCookie().
  // If signing up, then save to storage and setCookie().
  greet();
  showPage("home");
  adjustLayout();
});