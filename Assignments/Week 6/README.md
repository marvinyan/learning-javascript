Week 6 Assignment
---
The goal of this assignment was to improve the quiz made during week 5. 

- Quiz questions are now stored in an external JSON file.
- User authentication has been implemented. Users can now log in and save their credentials to local storage.
- A cookie is used to check if the user is logged in.
- A user's credentials are stored in localStorage as a JSON string (password, first name, and last name).

Note: There is no sign up form validation at the moment.

##Links
- <a href='http://jsfiddle.net/marvinyan/X9kMP/show/'>Page preview</a>
- <a href='http://jsfiddle.net/marvinyan/X9kMP/' target='_blank'>JSFiddle with code</a>
- [Learning Advanced JavaScript](http://ejohn.org/apps/learn/) by John Resig
- [The Past, Present, and Future of Local Storage for Web Applications](http://diveintohtml5.info/storage.html)